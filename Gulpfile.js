'use strict';
var gulp = require('gulp'),
    watch = require('gulp-watch'),
    wiredep = require('wiredep').stream,
    jshint = require('gulp-jshint'),
    jade = require('gulp-jade'),
    autoprefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    useref = require('gulp-useref'),
    _if = require('gulp-if'),
    cssmin = require('gulp-minify-css'),
    htmlmin = require('gulp-minify-html'),
    imagemin = require('gulp-imagemin'),
    git = require('gulp-git'),
    insertLines = require('gulp-insert-lines'),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),
    gulpSequence = require('gulp-sequence'),
    spritesmith = require('gulp.spritesmith');

gulp.task('jade', function(){
    return gulp.src('jade/*.jade')
        .pipe(jade({pretty: true}))
        .pipe(gulp.dest('.tmp'));
});

gulp.task('sass', function(){
    return gulp.src('sass/*.scss')
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 5 versions']
        }))
        .pipe(cssmin())
        .pipe(rename('style.css'))
        .pipe(gulp.dest('.tmp/css/'));
});

gulp.task('js', function(){
    return gulp.src('js/**/*.js')
        .pipe(jshint())
        .pipe(gulp.dest('.tmp/js'));
});

gulp.task('html', function(){

    return gulp.src(['jade/*.html', '.tmp/*.html'])
        .pipe(useref({ searchPath: '.tmp' }))
        .pipe(_if('*.js', uglify()))
        .pipe(_if('*.css', cssmin({compatibility: 'ie8'})))
        .pipe(useref())
        //.pipe(_if('*.html', htmlmin({conditionals: true, loose: true})))
        .pipe(gulp.dest('build'));

});

gulp.task('bower', function(){
    return gulp.src('./.tmp/*.html')
        .pipe(wiredep())
        .pipe(gulp.dest('./.tmp'));
});

gulp.task('rm-tmp', function(cb){
    rimraf('./.tmp', cb)
});

gulp.task('rm-build', function(cb){
    rimraf('./build', cb);
});

gulp.task('dev-watch', function(){
    gulp.watch('jade/**/*.jade', ['build']);

    gulp.watch('sass/**/*.scss', ['build']);

    gulp.watch('./bower.json', ['build']);

    gulp.watch('js/*.js', ['build']);
});

gulp.task('build', function(cb){
    gulpSequence('rm-build', 'sass', 'js', 'jade', 'bower', 'html', 'rm-tmp')(cb);
});

gulp.task('default', ['build', 'dev-watch']);